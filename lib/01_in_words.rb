class Fixnum
  def in_words
    return "zero" if self.zero?
    make_threes(self).split.join(" ")
  end
end

POWERS = {1 => "",
          1000 => " thousand",
          1_000_000 => " million",
          1_000_000_000 => " billion",
          1_000_000_000_000 => " trillion"}

def figure_magnitude(num)
  if num < 1000
    1
  elsif num >= 1000 && num < 1_000_000
    1000
  elsif num >= 1_000_000 && num < 1_000_000_000
    1_000_000
  elsif num >= 1_000_000_000 && num < 1_000_000_000_000
    1_000_000_000
  elsif num >= 1_000_000_000_000
    1_000_000_000_000
  end
end

def make_threes(num)
  magnitude = figure_magnitude(num)
  threes_num = num / magnitude
  if threes_num.zero?
    threes_str = partial_string(threes_num)
  else
    threes_str = partial_string(threes_num) + POWERS[magnitude]
  end
  return threes_str if (num % magnitude).zero?
  threes_str + " " + make_threes(num % magnitude)
end

def singles_string(num)
  ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"][num]
end

def teens_string(num)
  ["ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"][num]
end

def tens_string(num)
  ["", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"][num] + " "
end

def partial_string(num)
  hundreds = num / 100
  last_two = num % 100

  if hundreds.zero?
    hundreds_str = ""
  else
    hundreds_str = singles_string(hundreds) + " hundred"
    if last_two.zero?
      return hundreds_str
    end
  end

  if (0..9).include?(last_two)
    last_two_str = singles_string(last_two)
  elsif (10..19).include?(last_two)
    last_two_str = teens_string(last_two - 10)
  else
    last_two_str = tens_string(last_two / 10)
    last_two_str = last_two_str + singles_string(num % 10)
  end

  return hundreds_str + " " + last_two_str
end
